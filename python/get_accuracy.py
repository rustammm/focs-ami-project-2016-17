#!/usr/bin/env python
from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io
from skimage import color
import matplotlib.pyplot as plt
import argparse
import numpy
import pandas


def colour_match(a, b):
    if a[0] == b[0] and a[1] == b[1] and a[2] == b[2]:
        return True
    return False

def get_label(color):
    if colour_match(ground_truth[i][j] , [255, 255, 255]):
        return 0
    if colour_match(ground_truth[i][j] , [0, 0, 255]):
        return 1
    if colour_match(ground_truth[i][j] , [0, 255, 255]):
        return 2
    if colour_match(ground_truth[i][j] , [0, 255, 0]):
        return 3
    if colour_match(ground_truth[i][j], [255, 255, 0]):
        return 4
    return 5



ap = argparse.ArgumentParser()
ap.add_argument("-n", "--num", required = True, help = "Number to give")
args = vars(ap.parse_args())

# load the image and convert it to a floating point data type
img = io.imread("predicted_"+args["num"]+".tif")
ground_truth = io.imread("gt/area" +args["num"]+ ".tif")


h, w = len(img), len(img[0])

cnt_match = 0


for i in range(h):
    for j in range(w):
	cnt_match += colour_match(img[i][j], ground_truth[i][j])

print(cnt_match)
print("Accuracy is")
accuracy = float(cnt_match) / (h * w)
print(accuracy)

