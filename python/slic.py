#!/usr/bin/env python
# import the necessary packages
from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io
import matplotlib.pyplot as plt
import argparse
import numpy

numSegments = 10000

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "Path to the image")
ap.add_argument("-n", "--num", required = True, help = "Number to give")
args = vars(ap.parse_args())

# load the image and convert it to a floating point data type
image = img_as_float(io.imread(args["image"]))
print("image loaded")

segments = slic(image, n_segments = numSegments, sigma = 5, convert2lab = True)
segments2 = [[0 for j in range(len(segments[0]))] for i in range(len(segments))]

was_listed = [-1 for i in range(numSegments * 10)]
cnt_listed = 0

for i in range(len(segments)):
    for j in range(len(segments[i])):
        index = segments[i][j]
        if was_listed[index] == -1:
	    was_listed[index] = cnt_listed
	    cnt_listed = cnt_listed + 1
        segments2[i][j] = was_listed[index]




print("segmented")
savename = "segments_slic_" + args["num"] + ".csv"
numpy.savetxt(savename, segments2, delimiter=";", fmt="%d")


