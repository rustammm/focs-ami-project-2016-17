#!/usr/bin/env python
from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io
from skimage import color
import matplotlib.pyplot as plt
import argparse
import numpy
import pandas


def colour_match(a, b):
    if a[0] == b[0] and a[1] == b[1] and a[2] == b[2]:
        return True
    return False

def get_label(color):
    if colour_match(ground_truth[i][j] , [255, 255, 255]):
        return 0
    if colour_match(ground_truth[i][j] , [0, 0, 255]):
        return 1
    if colour_match(ground_truth[i][j] , [0, 255, 255]):
        return 2
    if colour_match(ground_truth[i][j] , [0, 255, 0]):
        return 3
    if colour_match(ground_truth[i][j], [255, 255, 0]):
        return 4
    return 5


numSegments = 11000

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "Path to the image")
ap.add_argument("-g", "--ground", required = True, help = "Ground truth")
ap.add_argument("-n", "--num", required = True, help = "Number to give")
ap.add_argument("-d", "--ndsm", required = True, help = "Path to the nDSM image")
args = vars(ap.parse_args())

# load the image and convert it to a floating point data type
img = io.imread(args["image"])
ground_truth = io.imread(args["ground"])
ndsm_img = io.imread(args["ndsm"])

filename = "segments_slic_" + args["num"] + ".csv"
segments = numpy.genfromtxt(filename, delimiter=';')



print("data_loaded")

grey_img = color.rgb2gray(img)
hsv_img  = color.rgb2hsv(img)
lab_img  = color.rgb2lab(img)
h, w = len(img), len(img[0])
colors = img




seg_count = [0 for i in range(numSegments)]


OTHERS_CNT = 11
INF = 100000000000

mean_others = [[0    for j in range(OTHERS_CNT)] for i in range(numSegments)]
diff_others = [[0    for j in range(OTHERS_CNT)] for i in range(numSegments)]
max_all     = [[-INF for j in range(OTHERS_CNT)] for i in range(numSegments)]
min_all     = [[INF  for j in range(OTHERS_CNT)] for i in range(numSegments)]
ground      = [0     for i in range(numSegments)]


max_index = 0

for i in range(h):
    for j in range(w):
        index = int(segments[i][j])
	max_index = max(max_index, index)
        seg_count[index] += 1
        ground[index] = get_label(ground_truth[i][j])

		

        others = colors[i][j].tolist() + hsv_img[i][j].tolist() + lab_img[i][j].tolist()
        others.append(grey_img[i][j])
        others.append(ndsm_img[i][j])
        others = numpy.array(others)

        for k in range(len(others)):
            mean_others[index][k] += others[k]
            max_all[index][k] = max(others[k], max_all[index][k])
            min_all[index][k] = min(others[k], min_all[index][k])



print("first loop completed")



for i in range(len(seg_count)):
    for j in range(len(mean_others[i])):
        mean_others[i][j] /= seg_count[i] + 1


for i in range(h):
    for j in range(w):
        index = int(segments[i][j])

        others = colors[i][j].tolist() + hsv_img[i][j].tolist() + lab_img[i][j].tolist()
        others.append(grey_img[i][j])
        others.append(ndsm_img[i][j])
        others = numpy.array(others)

        for k in range(len(others)):
            diff_others[index][k] += (mean_others[index][k] - others[k]) ** 2

print("second loop completed")

for i in range(len(seg_count)):
    for j in range(len(diff_others[i])):
        diff_others[i][j] /= seg_count[i] + 1


data = list()

temp_data = numpy.concatenate((mean_others, diff_others), axis=1)
temp_data = numpy.concatenate((temp_data, max_all),       axis=1)
temp_data = numpy.concatenate((temp_data, min_all),       axis=1)

for j in range(len(temp_data[0])):
    data.append([temp_data[i][j] for i in range(numSegments)])

#for i in range(OTHERS_CNT):
#    data.append(mean_others[i])
#    data.append(diff_others[i])
#    data.append(max_all[i])
#    data.append(min_all[i])
#    print(mean_others[i])
#    print(diff_others[i])
#    print(max_all[i])
#    print(min_all[i])
data.append(seg_count)
data.append(ground)

for i in range(len(data)):
    data[i] = data[i][0:max_index+1]

savename = "segments_data_" + args["num"] + ".csv"
numpy.savetxt(savename, data, delimiter=";", fmt="%1.6f")
