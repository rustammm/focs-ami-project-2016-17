#!/usr/bin/env python
from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io
from skimage import color
import matplotlib.pyplot as plt
import argparse
import numpy
import pandas


def colour_match(a, b):
    if a[0] == b[0] and a[1] == b[1] and a[2] == b[2]:
        return True
    return False

def get_label(color):
    if colour_match(ground_truth[i][j] , [255, 255, 255]):
        return 0
    if colour_match(ground_truth[i][j] , [0, 0, 255]):
        return 1
    if colour_match(ground_truth[i][j] , [0, 255, 255]):
        return 2
    if colour_match(ground_truth[i][j] , [0, 255, 0]):
        return 3
    if colour_match(ground_truth[i][j], [255, 255, 0]):
        return 4
    return 5


numSegments = 1000

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "Path to the image")
ap.add_argument("-g", "--ground", required = True, help = "Ground truth")
ap.add_argument("-n", "--num", required = True, help = "Number to give")
ap.add_argument("-d", "--ndsm", required = True, help = "Path to the nDSM image")
args = vars(ap.parse_args())

# load the image and convert it to a floating point data type
img = io.imread(args["image"])
ground_truth = io.imread(args["ground"])
ndsm_img = io.imread(args["ndsm"])

filename = "segments_slic_" + args["num"] + ".csv"
segments = numpy.genfromtxt(filename, delimiter=';')

print("data_loaded")

h, w = len(img), len(img[0])
colors = img




seg_count = [0 for i in range(numSegments)]


OTHERS_CNT = 11
INF = 100000000000

mean_others = [[0    for j in range(OTHERS_CNT)] for i in range(numSegments)]
diff_others = [[0    for j in range(OTHERS_CNT)] for i in range(numSegments)]
max_all     = [[-INF for j in range(OTHERS_CNT)] for i in range(numSegments)]
min_all     = [[INF  for j in range(OTHERS_CNT)] for i in range(numSegments)]
ground      = [0     for i in range(numSegments)]


max_index = 0

for i in range(0, h):
    for j in range(0, w):
        index = int(segments[i][j])
	


print("first loop completed")


