#!/usr/bin/env python
from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io
import matplotlib.pyplot as plt
import argparse
import numpy
import pandas
from sklearn.ensemble import RandomForestClassifier


ap = argparse.ArgumentParser()
ap.add_argument("-n", "--num", required = True, help = "Number of image")
args = vars(ap.parse_args())


IMG_NUM = args["num"]

classes = [[255, 255, 255], [0, 0, 255], [0, 255, 255], [0, 255, 0],  [255, 255, 0], [255, 0, 0]]

filename = "segments_slic_" + str(IMG_NUM) + ".csv"
segments = numpy.genfromtxt(filename, delimiter=';')

filename = "result_"+ str(IMG_NUM) +"_predicted.csv"
predicted = numpy.genfromtxt(filename, delimiter=';')

img = io.imread("top/top_mosaic_09cm_area"+str(IMG_NUM)+".tif")

for i in range(len(segments)):
    for j in range(len(segments[i])):
	index = int(predicted[int(segments[i][j])])
	if index > 5:
		index = 0
        img[i][j] = classes[index]

io.imsave("predicted_"+str(IMG_NUM)+".tif", img)
