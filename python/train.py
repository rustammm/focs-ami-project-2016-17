#!/usr/bin/env python
from skimage.segmentation import slic
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float
from skimage import io
import matplotlib.pyplot as plt
import argparse
import numpy
import pandas
from math import sqrt
from sklearn.ensemble import RandomForestClassifier

NUM_PARAMS = 45


print("loading data")

filename = "segments_data_3.csv"
segments = numpy.genfromtxt(filename, delimiter=';')

filename = "segments_data_5.csv"
segments2 = numpy.genfromtxt(filename, delimiter=';')

filename = "segments_data_17.csv"
segments3 = numpy.genfromtxt(filename, delimiter=';')

filename = "segments_data_11.csv"
segments4 = numpy.genfromtxt(filename, delimiter=';')

filename = "segments_data_13.csv"
segments5 = numpy.genfromtxt(filename, delimiter=';')

filename = "segments_data_21.csv"
segments6 = numpy.genfromtxt(filename, delimiter=';')

filename = "segments_data_23.csv"
segments7 = numpy.genfromtxt(filename, delimiter=';')

filename = "segments_data_26.csv"
segments8 = numpy.genfromtxt(filename, delimiter=';')

filename = "segments_data_28.csv"
segments9 = numpy.genfromtxt(filename, delimiter=';')

filename = "segments_data_30.csv"
segments10 = numpy.genfromtxt(filename, delimiter=';')

filename = "segments_data_37.csv"
segments11 = numpy.genfromtxt(filename, delimiter=';')


print("data loaded")

segments = segments.tolist()

for i in range(NUM_PARAMS + 1):
    segments[i] = segments[i] + segments2[i].tolist() + segments3[i].tolist() + segments4[i].tolist() + segments5[i].tolist() + segments6[i].tolist() + segments7[i].tolist() + segments8[i].tolist() + segments9[i].tolist() + segments10[i].tolist() # + segments11[i]

y = segments[NUM_PARAMS]
segments = segments[0:NUM_PARAMS]

#for i in range(NUM_PARAMS):
#    for  j in range(len(segments[i])):
#        segments[i][j] = int(segments[i][j])


data = list()
for j in range(len(segments[0])):
    data.append([segments[i][j] for i in range(NUM_PARAMS)])


for i in range(len(data)):
    if data[i][-1] == 0:
	y[i] = 6
        for j in range(len(data[i])):
	    data[i][j] = 0

print("training...")


rf = RandomForestClassifier(n_estimators=300)
rf.n_classes_ = 6
rf.n_features_ = NUM_PARAMS

rf.fit(data, y)


print("testing")

ap = argparse.ArgumentParser()
ap.add_argument("-n", "--num", required = True, help = "Image Number To Test")
args = vars(ap.parse_args())
num = args["num"]

filename = "segments_data_" + num +".csv"
segments2 = numpy.genfromtxt(filename, delimiter=';')

y = segments2[NUM_PARAMS]
segments2 = segments2[0:NUM_PARAMS]

data2 = list()
for j in range(len(segments2[0])):
    data2.append([segments2[i][j] for i in range(NUM_PARAMS)])

print("test_result")

for i in range(len(data2)):
    if data2[i][-1] == 0:
        for j in range(len(data2[i])):
	    data2[i][j] = 0


#ans = [rf.predict(data2[0]) for i in range(len(data2))]
ans = rf.predict(data2)
savename1 = "result_" + num +"_predicted.csv"
savename2 = "result_" + num +"_real.csv"
numpy.savetxt(savename1, ans, delimiter=";", fmt="%d")
numpy.savetxt(savename2, y, delimiter=";", fmt="%d")

error = 0
for i in range(len(y)):
    error += y[i] != ans[i]

print(error)
print(error/(len(y) + 0.0))

#filename = "segments_data_11.csv"
#segments3 = numpy.genfromtxt(filename, delimiter=';')
