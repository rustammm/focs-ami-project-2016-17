#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv.hpp"

#include "slic.h"
#include <iostream>
#include <fstream>
#include <cassert>
#include <algorithm>
#include <map>

using namespace cv;
using namespace std;


const int NUM_CLUSTERS = 1000;
vector < vector <double> > mean_disp;

vector <string> ERRORS;

// Classes: Imprevious Surfaces, Building, Low Vegetation, Tree, Car, Clutter/background
vector <CvScalar> classes_gt =
{ CV_RGB(255, 255, 255), CV_RGB(0, 0, 255) , CV_RGB(0, 255, 255),
  CV_RGB(0, 255, 0), CV_RGB(255, 255, 0), CV_RGB(255, 0, 0) };

const int NCLASSES = 6;

int get_class(CvScalar current, bool def = NCLASSES) {
    const double eps = 1e-1;

    for (int i = 0; i < NCLASSES; i++) {
        bool equal = true;
        for (int j = 0; j < 3; j++) {
            if (fabs(classes_gt[i].val[j] - current.val[j]) > eps)
                equal = false;
        }
        if (equal) return i;
    }
    return def;
}


vector <double> scalar_to_vector(CvScalar scalar) {
    return{ scalar.val[0], scalar.val[1], scalar.val[2] };
}

void add_color_params(CvScalar color, vector <double> &params) {
    params.push_back(color.val[0]);
    params.push_back(color.val[1]);
    params.push_back(color.val[2]);
    params.push_back(color.val[3]);
}

// Parametres for Random Forest
vector <double> get_params(int w, int h, IplImage *src, Slic *slic, IplImage *gt) {
    vector <double> params;

    // neighbours + self

    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            int w1 = w + i;
            int h1 = h + i;

            if (w1 < 0 || h1 < 0 || w1 >= src->width || h1 >= src->height) {
                add_color_params(cvGet2D(src, h, w), params);
                continue;
            }

            add_color_params(cvGet2D(src, h1, w1), params);
        }
    }




    // somehow get dsm ???


    // cluster info
    int index = slic->get_cluster(w, h);

    CvScalar mean_cluster = CV_RGB(0, 0, 0);
    double mean_gray = 0, dis_grey = 0;
    double dispersion = 0;
    if (mean_disp[index][0] == -1) {
        for (int i = 0; i < src->width; i++) {
            for (int j = 0; j < src->height; j++) {
                if (slic->get_cluster(i, j) == index) {
                    CvScalar color = cvGet2D(src, j, i);
                    mean_cluster.val[0] += color.val[0];
                    mean_cluster.val[1] += color.val[1];
                    mean_cluster.val[2] += color.val[2];
                    int gc = color.val[0] * 0.299 + color.val[1] * 0.587 + color.val[2] * 0.114;
                    mean_gray += gc;
                }
            }
        }
        mean_cluster.val[0] /= slic->get_center_counts(index);
        mean_cluster.val[1] /= slic->get_center_counts(index);
        mean_cluster.val[2] /= slic->get_center_counts(index);
        mean_gray /= slic->get_center_counts(index);

        for (int i = 0; i < src->width; i++) {
            for (int j = 0; j < src->height; j++) {
                if (slic->get_cluster(i, j) == index) {
                    CvScalar col = cvGet2D(src, j, i);
                    double dist = 0;
                    dist += (col.val[0] - mean_cluster.val[0]) * (col.val[0] - mean_cluster.val[0]);
                    dist += (col.val[1] - mean_cluster.val[1]) * (col.val[1] - mean_cluster.val[1]);
                    dist += (col.val[2] - mean_cluster.val[2]) * (col.val[2] - mean_cluster.val[2]);
                    dispersion += dist;
                    int gc = col.val[0] * 0.299 + col.val[1] * 0.587 + col.val[2] * 0.114;
                    dis_grey += (mean_gray - gc) * (mean_gray - gc);

                }
            }
        }
        dispersion /= slic->get_center_counts(index);

        mean_disp[index][0] = mean_cluster.val[0];
        mean_disp[index][1] = mean_cluster.val[1];
        mean_disp[index][2] = mean_cluster.val[2];
        mean_disp[index][3] = dispersion;
        mean_disp[index][4] = mean_gray;
        mean_disp[index][5] = dis_grey;

    } else {
        mean_cluster.val[0] = mean_disp[index][0];
        mean_cluster.val[1] = mean_disp[index][1];
        mean_cluster.val[2] = mean_disp[index][2];
        dispersion = mean_disp[index][3];
        mean_gray = mean_disp[index][4];
        dis_grey = mean_disp[index][5];
    }

    add_color_params(mean_cluster, params);
    params.push_back(dispersion);
    params.push_back(mean_gray);
    params.push_back(dis_grey);

    // ground truth
    CvScalar colour = cvGet2D(gt, h, w);
    int class_index = get_class(colour, 0);
    params.push_back(class_index);


    return params;
}

vector <vector <double> > get_training_set(Slic *slic, IplImage *src, IplImage *gt) {
    mean_disp.resize(NUM_CLUSTERS + 10, vector <double> (6, -1));
    map <int, bool> viewed_superpixel;
    vector <vector <double> > training_set;
    for (int i = 0; i < src->width; i++) {
        for (int j = 0; j < src->height; j++) {
            int index = slic->get_cluster(i, j);
            if (index == -1) continue;
            const vector <double> v = get_params(i, j, src, slic, gt);

            training_set.push_back(v);
        }
    }

    return training_set;
}




int main(int argc, const char** argv)
{
    IplImage *src = cvLoadImage("ortho.tif");
    IplImage *ground_truth = cvLoadImage("ground_truth.tif");
    Mat img = cvarrToMat(src);
    IplImage *lab_image = cvCloneImage(src);
    cvCvtColor(src, lab_image, CV_BGR2Lab);

    int w = src->width, h = src->height;
    int nr_superpixels = NUM_CLUSTERS;
    int nc = 20;

    double step = sqrt((w * h) / (double)nr_superpixels);

    /* Perform the SLIC superpixel algorithm. */
    Slic slic;
    slic.generate_superpixels(lab_image, step, nc);
    slic.create_connectivity(lab_image);



    ofstream set_file("training_set.txt");
    vector <vector <double> > tset = get_training_set(&slic, src, ground_truth);
    for (int i = 0; i < tset.size(); i++) {
        for (int j = 0; j < tset[i].size(); j++)
            set_file << tset[i][j] << " ";
        set_file << endl;
    }


    slic.colour_with_cluster_means(lab_image);
    slic.display_contours(lab_image, CvScalar(CV_RGB(255, 0, 0)));
    // vector <double> error = slic.calc_error(src, ground_truth);


	namedWindow("MYWINDOW", CV_WINDOW_AUTOSIZE);
	imshow("MYWINDOW", cvarrToMat(src));


	waitKey();

	return 0;
}
