#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/opencv.hpp"

#include "slic.h"
#include <iostream>
#include <fstream>
#include <cassert>
#include <algorithm>
#include <map>



using namespace cv;
#include "opencv2/ml.hpp"
using namespace std;



vector <string> ERRORS;

// Classes: Imprevious Surfaces, Building, Low Vegetation, Tree, Car, Clutter/background
vector <CvScalar> classes_gt;
const int NCLASSES = 6;

int get_class(CvScalar current, bool def = NCLASSES) {
    const double eps = 1e-1;

    for (int i = 0; i < NCLASSES; i++) {
        bool equal = true;
        for (int j = 0; j < 3; j++) {
            if (fabs(classes_gt[i].val[j] - current.val[j]) > eps)
                equal = false;
        }
        if (equal) return i;
    }
    return def;
}


vector <double> scalar_to_vector(CvScalar scalar) {
    vector <double> res;
    res.push_back(scalar.val[0]);
    res.push_back(scalar.val[1]);
    res.push_back(scalar.val[2]);
    return res;
}


// Peresegmentation Error
vector <double> Slic::calc_error(IplImage *image, IplImage *gt, string out_file) {
    int nr_superpixels = centers.size();
    vec2di gt_on_clusters;
    gt_on_clusters.resize(nr_superpixels + 1, vector <int>(6));
    int cnt_bad_index = 0;

    for (int i = 0; i < min(image->width, gt->width); i++) {
        for (int j = 0; j < min(image->height, gt->height); j++) {
            int index = clusters[i][j];
            if (index < 0) {
                ERRORS.push_back("index in calc_error out of range(<0)");
                cnt_bad_index++;
                continue;
            }
            assert(index >= 0 && index < gt_on_clusters.size());
            CvScalar colour = cvGet2D(gt, j, i);
            int class_index = get_class(colour, 0);

            gt_on_clusters[index][class_index]++;
        }
    }


    vector <double> error;

    for (int i = 0; i < nr_superpixels; i++) {
        sort(gt_on_clusters[i].begin(), gt_on_clusters[i].end());
        int max_class_val = gt_on_clusters[i].back();
        error.push_back(1 - (double)max_class_val / center_counts[i]);
    }

    // printing error

    double mean = 0;
    ofstream fout(out_file.c_str());
    for (int i = 0; i < error.size(); i++) {
        mean += error[i];
        fout << "Error on cluster [" << i << "] = " << error[i] << endl;
    }
    fout << "Mean Error is " << mean / error.size() << endl;
    fout << "Bad indeces count: " << cnt_bad_index << endl;

    return error;
}


// Ex 1


// Segmentation Accuracy
void get_accuracy(IplImage *segmented, IplImage *ground_truth) {
    ofstream fout("accuracy.txt");
    vector <vector <int> > cnt(NCLASSES + 1, vector <int>(NCLASSES + 1)); // matrix (prediction/ground truth)

    assert(segmented->width == ground_truth->width);
    assert(segmented->height == ground_truth->height);

    int total = segmented->width * segmented->height;

    for (int i = 0; i < segmented->width; i++) {
        for (int j = 0; j < segmented->height; j++) {
            cnt[get_class(cvGet2D(segmented, j, i))][get_class(cvGet2D(ground_truth, j, i))]++;
        }
    }

    double sum_good = 0;
    for (int i = 0; i < NCLASSES; i++) {
        sum_good += cnt[i][i];
        int TP = cnt[i][i];
        int FN = 0;
        int FP = 0;
        int TN = 0;

        for (int j = 0; j < NCLASSES; j++) {
            if (i == j) continue;
            FN += cnt[j][i];
        }

        for (int j = 0; j < NCLASSES; j++) {
            if (i == j) continue;
            FP += cnt[i][j];
        }

        TN = total - TP - FN - FP;

        double precision = (double)TP / (TP + FP);
        double recall = (double)TP / (TP + FN);
        double f1_score = 2 * precision * recall / (precision + recall);

        fout << "Class N " << i << endl;
        fout << "Precision: " << precision << endl;
        fout << "Recall:    " << recall << endl;
        fout << "F1 Score:  " << f1_score << endl;
    }

    fout << "Overall accuracy is " << sum_good / total << endl;
}


void add_color_params(CvScalar color, vector <double> &params) {
    params.push_back(color.val[0]);
    params.push_back(color.val[1]);
    params.push_back(color.val[2]);
}

// Parametres for Random Forest
vector <double> get_params(int w, int h, IplImage *src, Slic *slic) {
    vector <double> params;



    // neighbours + self

    for (int i = -1; i <= 1; i++) {
        for (int j = -1; j <= 1; j++) {
            int w1 = w + i;
            int h1 = h + i;

            if (w1 < 0 || h1 < 0 || w1 >= src->width || h1 >= src->height)
                continue;

            add_color_params(cvGet2D(src, h1, w1), params);
        }
    }


    // somehow get dsm ???


    // cluster info
    int index = slic->get_cluster(w, h);
    CvScalar mean_cluster = CV_RGB(0, 0, 0);
    double dispersion = 0;
    if (index != -1) {
        for (int i = 0; i < src->width; i++) {
            for (int j = 0; j < src->height; j++) {
                if (slic->get_cluster(i, j) == index) {
                    CvScalar color = cvGet2D(src, j, i);
                    mean_cluster.val[0] += color.val[0];
                    mean_cluster.val[1] += color.val[1];
                    mean_cluster.val[2] += color.val[2];
                }
            }
        }
        mean_cluster.val[0] /= slic->get_center_counts(index);
        mean_cluster.val[1] /= slic->get_center_counts(index);
        mean_cluster.val[2] /= slic->get_center_counts(index);


        for (int i = 0; i < src->width; i++) {
            for (int j = 0; j < src->height; j++) {
                if (slic->get_cluster(i, j) == index) {
                    CvScalar col = cvGet2D(src, j, i);
                    double dist = 0;
                    dist += (col.val[0] - mean_cluster.val[0]) * (col.val[0] - mean_cluster.val[0]);
                    dist += (col.val[1] - mean_cluster.val[1]) * (col.val[1] - mean_cluster.val[1]);
                    dist += (col.val[2] - mean_cluster.val[2]) * (col.val[2] - mean_cluster.val[2]);
                    dispersion += dist;
                }
            }
        }
        dispersion /= slic->get_center_counts(index);
    }

    add_color_params(mean_cluster, params);
    params.push_back(dispersion);

    return params;
}

Mat get_training_set(Slic *slic, IplImage *src) {
    Mat training_set;
    for (int i = 0; i < src->width; i++) {
        for (int j = 0; j < src->height; j++) {
            const vector <double> v = get_params(i, j, src, slic);
            Mat line(v);

            training_set.push_back(v);
        }
    }

    return training_set;
}

/*
// Doesn't work
void do_trees(Slic *slic, IplImage *src) {
    Mat training_set = get_training_set(slic, src);




    CvRTrees tree;
    CvRTParams cvrtp; // set params, play with those!! (the numbers were for my problem)
    cvrtp.max_depth = 10;
    cvrtp.min_sample_count = 6;
    cvrtp.max_categories = 6;
    cvrtp.term_crit.max_iter = 100;

    tree.train(train_features, CV_ROW_SAMPLE, train_labels, cv::Mat(), cv::Mat(), cv::Mat(), cv::Mat(), cvrtp);

    // later:
    float id = tree.predict(sample); // where sample is a lbp feature , again



}
*/

int main(int argc, const char** argv)
{
    classes_gt.push_back(CV_RGB(255, 255, 255));
    classes_gt.push_back(CV_RGB(0, 0, 255));
    classes_gt.push_back(CV_RGB(0, 255, 255));
    classes_gt.push_back(CV_RGB(0, 255, 0));
    classes_gt.push_back(CV_RGB(255, 255, 0));
    classes_gt.push_back(CV_RGB(255, 0, 0));




	IplImage *src = cvLoadImage("ortho.tif");
    IplImage *ground_truth = cvLoadImage("ground_truth.tif");
	Mat img = cvarrToMat(src);
	IplImage *lab_image = cvCloneImage(src);
	cvCvtColor(src, lab_image, CV_BGR2Lab);

	int w = src->width, h = src->height;
	int nr_superpixels = 10000;
	int nc = 20;

	double step = sqrt((w * h) / (double)nr_superpixels);

	/* Perform the SLIC superpixel algorithm. */
	Slic slic;
	slic.generate_superpixels(lab_image, step, nc);
	slic.create_connectivity(lab_image);


    vector <double> error = slic.calc_error(src, ground_truth, "slic_error.txt");


	namedWindow("MYWINDOW", CV_WINDOW_AUTOSIZE);
	//Mat ret = cvarrToMat(dst);
	imshow("MYWINDOW", cvarrToMat(src));

	waitKey();

	return 0;
}
